import json
import subprocess
import os
from flask import Flask, request

app = Flask(__name__)

@app.route("/", methods=['POST'])
def hello():
    with open('config.json') as config_file:
        config = json.load(config_file)
        
    res = request.get_json()

    # BITBUCKET JSON
    push_branch = res['push']['changes'][0]['new']['name']
    push_repo_name = res['repository']['name']

    for repo in config['repositories']:
        if push_branch == repo['branch'] and push_repo_name == repo['name']:
            os.chdir(repo['path'])
            output = subprocess.check_output(repo['command'], shell=True)
            print(output)

    return ":D"

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
